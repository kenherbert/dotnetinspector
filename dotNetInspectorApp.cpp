//---------------------------------------------------------------------------
//
// Name:        dotNetInspectorApp.cpp
// Author:      Firedancer Software
// Created:     6/05/2012 1:48:36 PM
// Description:
//
//---------------------------------------------------------------------------

#include "dotNetInspectorApp.h"
#include "dotNetInspectorFrm.h"
#include <wx/protocol/http.h>
#include <wx/stdpaths.h>

IMPLEMENT_APP(dotNetInspectorFrmApp)

bool dotNetInspectorFrmApp::OnInit()
{
	parser.SetDesc(g_cmdLineDesc);
	parser.SetCmdLine(argc, argv);
	parser.SetSwitchChars(wxT("-"));
	int iParseVal = parser.Parse();

	if(iParseVal != 0)
	{
		return 0;
	}

	wxString appPath = wxStandardPaths::Get().GetExecutablePath();
	defaultExportPath = wxPathOnly(appPath);
	parser.Found(wxT("e"), &defaultExportPath);

	wxHTTP::Initialize();
	dotNetInspectorFrm* frame = new dotNetInspectorFrm(defaultExportPath);
	SetTopWindow(frame);

	frame->SetIcon(wxICON(appicon));
	frame->Show();
	return true;
}

int dotNetInspectorFrmApp::OnExit()
{
	return 0;
}

